
#!/bin/bash

# avoid  the sudo unable to resolve
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts


# /etc/hosts config
cp -p /etc/hosts /etc/hosts.DIST
sed -i 1i$'192.168.61.40    gateway' /etc/hosts
sed -i 1i$'192.168.61.33    node03' /etc/hosts
sed -i 1i$'192.168.61.32    node02' /etc/hosts
sed -i 1i$'192.168.61.31    node01' /etc/hosts


