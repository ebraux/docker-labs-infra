


getsion des ports pour swarm

TCP port 2377 for cluster management communications
TCP and UDP port 7946 for communication among nodes
UDP port 4789 for overlay network traffic

If you plan on creating an overlay network with encryption (--opt encrypted), you also need to ensure ip protocol 50 (ESP) traffic is allowed.


---
# getsion de l'environnement de TP

vagrant status
Current machine states:

manager1                  running (virtualbox)
manager2                  running (virtualbox)
worker1                   running (virtualbox)
worker2                   running (virtualbox)
nfs1                      running (virtualbox)

vagrant status  manager1
Current machine states:

manager1                  running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to
shut it down forcefully, or you can run `vagrant suspend` to simply
suspend the virtual machine. In either case, to restart it again,
simply run `vagrant up`.


ssh -l cloud -p 2231 localhost
sudo hostnamectl set-hostname manager2

---

Se connecter sur `manager1`, et afficher l'état du cluster :
``` bash
docker node ls
# Error response from daemon: This node is not a swarm manager. Use "docker swarm init" or "docker swarm join" to connect this node to swarm and try again.
```

Le noeud docker n'appartient pas à un cluster, il faut donc initialiser swarm : 
``` bash
docker swarm init
# Error response from daemon: could not choose an IP address to advertise since this system has multiple addresses on different interfaces (10.0.2.15 on enp0s3 and 172.16.50.21 on enp0s8) - specify one with --advertise-addr
```

Dans le cas d'un déploiement sur une machine avec plusieurs IP, il faut préciser quelle est l'IP utilisée pour la communication entre les noeuds :  
``` bash
docker swarm init --advertise-addr 172.16.50.21
# Swarm initialized: current node (iz3s6lc49xsy0zohk66slk923) is now a manager.
# 
# To add a worker to this swarm, run the following command:
# 
#    docker swarm join --token xxxxxxxxxxxxxxxxxxxxx  172.16.50.21:2377
# 
# To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

``` bash
docker node ls
# ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
# iz3s6lc49xsy0zohk66slk923 *   manager1   Ready     Active         Leader           20.10.19
```
On a donc un cluster de 1 noeud

---
## Ajouter un noeud au cluster

Pour ajouter un noeud au cluster, on utilise la commande `docker swarm join`, en précisant un token, et la chaîne de connexion à un manager.

Il existe un token pour rejoindre le cluster en tant que `manager`, et un autre pour le rejoindre en tant que `worker`

Afficher le token pour ajouter un manager : 
``` bash
docker swarm join-token manager
# To add a manager to this swarm, run the following command:
# 
#    docker swarm join --token MMMMMMMMMMMMMMMMMM 172.16.50.21:2377
```
> docker swarm join --token SWMTKN-1-0sd1ela7ebppcsxc1xftqxp7fxf7fx90qoww4dy1w6j3ft11zg-bdo9zbw8n7r9lilzg21qr9z4b 172.16.50.21:2377


Afficher le token pour ajouter un Worker : 
``` bash
docker swarm join-token worker
# To add a worker to this swarm, run the following command:
#    docker swarm join --token WWWWWWWWWWWWWWWW  172.16.50.21:2377
```
> docker swarm join --token SWMTKN-1-0sd1ela7ebppcsxc1xftqxp7fxf7fx90qoww4dy1w6j3ft11zg-2afaoqaw19k70g6dk5a3phjpp 172.16.50.21:2377


Se connecter sur `manager2`, vérifier qu'il n'appartient à aucun cluster, l'intégrer au cluster fraichement créée en tant que manager, et vérifier :

``` bash
docker node ls
```

``` bash
docker swarm join --token MMMMMMMMMMMMMMMMMM 172.16.50.21:2377
# This node joined a swarm as a manager.
```

``` bash
docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
iz3s6lc49xsy0zohk66slk923 *   manager1   Ready     Active         Leader           20.10.19
b8mbjtc2g6br86d968cyr0zxm     manager2   Ready     Active         Reachable        20.10.19
```

On a donc un manager avec le status `Leader`, et un autre avec le status `Reachable`


Passe run manager en worker
docker swarm denote
---
``` bash
docker node ls
```

docker swarm join-token 
docker swarm join-token  manager

journalctl -f -u docker.service


Basculer un node de worker en manager

On ne peut pas changer le leaser. c'est Swarm qui géère automatiquement.
- rebooter le leader, c'est un autre manager qui devient leader


---
######################
# TP8 - docker-swarm #
######################


# Partie 1 - Création d'un cluster

Mettre en place un cluster swarm de 3 noeuds sur les machines vierges fournies
- installer docker
- initialiser le swarm sur un noeud
- rejoindre le swarm sur les deux autres noeuds
- vérifier que les 3 noeuds forment bien un cluster
- quel noeud est le leader ?
- stopper docker sur un noeud et observer le comportement du cluster
- redémarrer docker sur ce noeud


# Partie 2 - Déploiement d'une application et accessibilité

- créer une stack "demo1" avec un container accessible sur le port 80 et vérifier que vous pouvez y accéder
par chacune des IPs publiques du cluster
- ouvrir 3 fenêtres et y exécuter la commande suivante:
watch -n 1 wget http://IP_CLUSTER


- arrêter docker sur un noeud et observer le comportement


# Partie 3 - Déploiement d'une application 1

# A

Déployer une stack demo2 avec les services suivant:
- web (nginx ou httpd par exemple)


# B

Comment faire pour servir le même contenu sur chacun des containers ?


# Partie 4 - Déploiement d'une application 2

Déployer une stack demo3 avec les services suivant:
- web (phpmyadmin, wordpress...)
- bdd (mysql)

Quelles difficultés voyez vous pour le scaling des composants BDD / Web ?

- Scaler le composant Web up/down
- Scaler le composant BDD up/down


# Partie 4 - Gestion des contraintes

Ajouter des labels

docker node update --label-add $label $name
docker node ls -q | xargs docker node inspect -f '{{ .ID }} [{{ .Description.Hostname }}]: {{ range $k, $v := .Spec.Labels }}{{ $k }}={{ $v }} {{end}}'

"docker node ls -q | xargs docker node inspect -f '{{ .ID }} [{{ .Description.Hostname }}]: {{ range \$k, \$v := .Spec.Labels }}{{ \$k }}={{ \$v }} {{end}}' | grep $name | grep $label",

- max_replicas_per_node
- placement constraints


---


docker stack ls


creer le service.
- vérifier que le port 80 est bien ouvert sur tous les ports
- tester l'accès via l'IP publique de tous les managers

Attention, le  pour le traffic overlay, le traffic se fait en UDP !
Il faut êtr certain d'avoir bien ouver


stoppepr le noeud qui héberge lecontainer du service. observer
- le neud est arr^té
- le container a donc été arrêté
- automatiquement le cluster redéploie le container


Autre test : montrer qu'avec un seul container, pendant la relance du container, le service ne répond pas.
- observer sur quelle machine tourne le container
- faire des requetes avec la commande "watch", + un wget qui fait une requete toutes les secondes sur un noeud manager qui n'heberge pas le service 
- arrêter le neoud qui héberge
- observer le temps de relance : chargement d el'image + démarrage
 Solution : avoir plusisuers container : faire un deploy, avec 3 noeuds

Si on a plus de repliqautes, que de nodes disponible : docker déploie le nombre d eréplicats demendé : pas optimum

---
## contraintes

Options de deploy : placement max replicat per node
- Si on met 1 replicat êr node. swarm déplioe ce qu'il peut, mais ensuite il marque les services en "no suitable node ..."

deploy / placement / constraints
  node.role = worker

Docker node update label add ..
- pour voir les label : docker node inspect
- node.label ??

https://www.sweharris.org/post/2017-07-30-docker-placement/
On peut faire est, ou n'est pas :
https://blog.raveland.tech/post/constraints_swarm/


redéploiment forcé :
- scale up/down
- update --force

outil : visualizer, ....
https://superuser.com/questions/1534571/docker-swarm-usage-of-constraint-for-production-and-development
