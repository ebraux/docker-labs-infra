 #!/bin/bash

# enable password Authentification, and set ubuntu password
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "ubuntu:cloud" | chpasswd

# avoid  the sudo unable to resolve
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts


# set environement for installation and non interactive update
export DEBIAN_FRONTEND=noninteractive
export TERM="xterm"

# set the system up to date
apt-get update && apt -y upgrade && apt-get clean

# Gestion de Repositories complementaires
apt install -y  software-properties-common

# Outils standards
apt install -y sudo vim  lynx zip binutils wget openssl ssl-cert ssh nano


# creation de l'utilisateur cloud
groupadd cloud
useradd -g cloud       -s /bin/bash -d /home/cloud  -m  cloud
cd /etc/sudoers.d
echo "cloud ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_cloud_sh
chmod 440 /etc/sudoers.d/50_cloud_sh


# Installation de Docker
sudo apt-get remove -y docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
rm -f /etc/apt/keyrings/docker.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo chmod a+r /etc/apt/keyrings/docker.gpg
sudo apt-get update
sudo apt-get install -y \
     docker-ce docker-ce-cli \
     containerd.io \
     docker-compose-plugin

# installation de ab
apt install -y apache2-utils

sudo apt autoremove -y