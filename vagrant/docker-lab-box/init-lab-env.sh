
#!/bin/bash


# Avoid  the sudo unable to resolve
echo $(hostname -I | cut -d\  -f1) $(hostname) | sudo tee -a /etc/hosts

# Configure lab DNS env
cp -p /etc/hosts /etc/hosts.DIST
sed -i 1i$'172.16.50.31    manager1' /etc/hosts
sed -i 1i$'172.16.50.32    manager2' /etc/hosts
sed -i 1i$'172.16.50.41    worker1'  /etc/hosts
sed -i 1i$'172.16.50.42    worker2'  /etc/hosts
sed -i 1i$'172.16.50.43    worker3'  /etc/hosts
sed -i 1i$'172.16.50.51    nfs1'     /etc/hosts