

Mettre à jour la VM de base (optionnel)
``` bash
vagrant box update --box ubuntu/jammy64
```

Lancer la VM
``` bash
vagrant up
```

Une fois la VM lancée et initialisée, vérifier son stastus, et l'arrêter

``` bash
vagrant status
# dockerlabbox              running (virtualbox)
```

``` bash
vagrant halt
```

``` bash
vagrant status
# dockerlabbox              poweroff (virtualbox)
```

Quand la VM est arrêtée, si besoin supprimer l'ancien package, et  créer un package "BOX"

``` bash
if [ -f docker-lab.box ]; then rm docker-lab.box; fi
```

``` bash
vagrant package  \
  --base dockerlabbox \
  --info info.json \
  --output docker-lab.box \
  --vagrantfile Vagrantfile \
  --include metadata.json,init-lab-env.sh,add-lab-docker.sh,init-cloud-user.sh
# --> docker-lab.box    
```

Puis ajouter la box en local  

- Vérifier qu'une box n'est pas déjà présente
``` bash
vagrant box list
```
- Si necessaire
``` bash
vagrant box remove docker-lab
```
- Et enfin ajouter la box
``` bash
vagrant box add docker-lab file://docker-lab.box
```
- Vérifier l'import
``` bash
vagrant box list -i 
```

Une fois l'import validé, Ménage  :
``` bash
vagrant destroy
```
