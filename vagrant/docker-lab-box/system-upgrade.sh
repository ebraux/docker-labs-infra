#!/bin/bash


# set environement for installation and non interactive update
export DEBIAN_FRONTEND=noninteractive
export TERM="xterm"

# Set up the repository
# - Update the apt package index and install packages
apt update
apt upgrade -y
apt clean
apt auto-remove -y