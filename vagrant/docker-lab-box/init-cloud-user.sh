#!/bin/bash

# enable password Authentification, and set ubuntu password
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
service sshd restart
echo "ubuntu:cloud" | chpasswd


# creation de l'utilisateur cloud
if [ ! $(getent group cloud) ]; then
    groupadd cloud
fi

if [ ! $(getent passwd cloud) ]; then
    useradd -g cloud       -s /bin/bash -d /home/cloud  -m  cloud
    cd /etc/sudoers.d
    echo "cloud ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/50_cloud_sh
    chmod 440 /etc/sudoers.d/50_cloud_sh
    echo "cloud:cloud" | chpasswd
fi

# autoriser l'utilisateur Cloud a utiliser Docker, si docker est installé
if [ $(getent group docker) ]; then sudo usermod -aG docker cloud; fi